﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TuermeVonHanoi;

namespace ConsoleApplication {
    class Program {
        static void Main(string[] args)
        {
            Hanoi hanoi = new Hanoi(5);
            HanoiSolver solver = new HanoiSolver(hanoi);

            solver.Solve();
            foreach(HanoiMove m in solver.Moves)
            {
                Console.WriteLine(String.Format("Move {0} from {1} to {2}",m.Disk, m.From, m.To));
            }
            Console.ReadLine();
        }
    }
}
