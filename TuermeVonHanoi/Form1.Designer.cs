﻿namespace TuermeVonHanoi {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.towerA = new System.Windows.Forms.TableLayoutPanel();
            this.towerC = new System.Windows.Forms.TableLayoutPanel();
            this.towerB = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnSolve = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.lblMoveCount = new System.Windows.Forms.Label();
            this.lblLastMove = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // towerA
            // 
            this.towerA.AutoSize = true;
            this.towerA.BackColor = System.Drawing.SystemColors.Control;
            this.towerA.ColumnCount = 1;
            this.towerA.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.towerA.Location = new System.Drawing.Point(12, 65);
            this.towerA.Name = "towerA";
            this.towerA.RowCount = 5;
            this.towerA.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.towerA.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.towerA.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.towerA.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.towerA.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.towerA.Size = new System.Drawing.Size(300, 104);
            this.towerA.TabIndex = 0;
            // 
            // towerC
            // 
            this.towerC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.towerC.AutoSize = true;
            this.towerC.ColumnCount = 1;
            this.towerC.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.towerC.Location = new System.Drawing.Point(737, 65);
            this.towerC.Name = "towerC";
            this.towerC.RowCount = 1;
            this.towerC.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.towerC.Size = new System.Drawing.Size(300, 104);
            this.towerC.TabIndex = 1;
            // 
            // towerB
            // 
            this.towerB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.towerB.AutoSize = true;
            this.towerB.ColumnCount = 1;
            this.towerB.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.towerB.Location = new System.Drawing.Point(386, 65);
            this.towerB.Name = "towerB";
            this.towerB.RowCount = 1;
            this.towerB.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.towerB.Size = new System.Drawing.Size(300, 104);
            this.towerB.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(308, 50);
            this.button1.TabIndex = 2;
            this.button1.Text = "Hierhin verschieben";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(386, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(308, 50);
            this.button2.TabIndex = 3;
            this.button2.Text = "Hierhin verschieben";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(729, 12);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(308, 50);
            this.button3.TabIndex = 4;
            this.button3.Text = "Hierhin verschieben";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnSolve
            // 
            this.btnSolve.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSolve.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSolve.Location = new System.Drawing.Point(12, 393);
            this.btnSolve.Name = "btnSolve";
            this.btnSolve.Size = new System.Drawing.Size(308, 34);
            this.btnSolve.TabIndex = 5;
            this.btnSolve.Text = "schnellste Lösung berechnen";
            this.btnSolve.UseVisualStyleBackColor = true;
            this.btnSolve.Click += new System.EventHandler(this.btnSolve_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(12, 349);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(308, 34);
            this.btnReset.TabIndex = 6;
            this.btnReset.Text = "Zurücksetzen";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lblMoveCount
            // 
            this.lblMoveCount.AutoSize = true;
            this.lblMoveCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoveCount.Location = new System.Drawing.Point(326, 398);
            this.lblMoveCount.Name = "lblMoveCount";
            this.lblMoveCount.Size = new System.Drawing.Size(140, 24);
            this.lblMoveCount.TabIndex = 7;
            this.lblMoveCount.Text = "Anzahl Schritte:";
            this.lblMoveCount.Click += new System.EventHandler(this.lblMoveCount_Click);
            // 
            // lblLastMove
            // 
            this.lblLastMove.AutoSize = true;
            this.lblLastMove.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastMove.Location = new System.Drawing.Point(326, 354);
            this.lblLastMove.Name = "lblLastMove";
            this.lblLastMove.Size = new System.Drawing.Size(126, 24);
            this.lblLastMove.TabIndex = 8;
            this.lblLastMove.Text = "Letzter Schritt:";
            this.lblLastMove.Click += new System.EventHandler(this.lblLastMove_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 439);
            this.Controls.Add(this.lblLastMove);
            this.Controls.Add(this.lblMoveCount);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSolve);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.towerB);
            this.Controls.Add(this.towerC);
            this.Controls.Add(this.towerA);
            this.Name = "Form1";
            this.Text = "Türme von Hanoi";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel towerA;
        private System.Windows.Forms.TableLayoutPanel towerC;
        private System.Windows.Forms.TableLayoutPanel towerB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnSolve;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label lblMoveCount;
        private System.Windows.Forms.Label lblLastMove;
    }
}

