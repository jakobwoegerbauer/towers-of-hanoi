﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuermeVonHanoi {
    public class Hanoi {
        public int N { get; private set; }
        private Stack<int>[] towers;

        public Hanoi(int N)
        {
            this.N = N;
            Reset();
        }

        public void Reset()
        {
            Stack<int> disks = new Stack<int>();
            for(int i = N; i >= 1; i--)
            {
                disks.Push(i);
            }
            towers = new Stack<int>[3];
            towers[(int)Tower.A] = disks;
            towers[(int)Tower.B] = new Stack<int>();
            towers[(int)Tower.C] = new Stack<int>();
        }

        public int Move(Tower from, Tower to)
        {
            //Console.WriteLine("from " + from + " to " + to);
            if(towers[(int)from].Count == 0)
            {
                return -1;
            }
            if(towers[(int)to].Count != 0 && towers[(int)from].Peek() > towers[(int)to].Peek())
            {
                return -2;
            }
            int disk = towers[(int)from].Pop();
            towers[(int)to].Push(disk);
            return disk;
        }

        public bool HasDisk(Tower tower, int disk)
        {
            return towers[(int)tower].Contains(disk);
        }

        public int TowerCount(Tower tower)
        {
            return towers[(int)tower].Count();
        }

        public List<int> GetDisks(Tower tower)
        {
            List<int> disks = new List<int>();
            while(!(towers[(int)tower].Count() == 0))
            {
                disks.Add(towers[(int)tower].Pop());
            }
            disks.Reverse();
            foreach(int disk in disks)
            {
                towers[(int)tower].Push(disk);
            }
            return disks;
        }

        public bool IsFinished()
        {
            return towers[(int)Tower.C].Count == N;
        }
    }
}
