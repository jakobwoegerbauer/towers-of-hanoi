﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TuermeVonHanoi {
    public partial class Form1 : Form {
        private int N = 4;
        private Hanoi hanoi;
        private HanoiSolver solver;
        private Tower? dragDropSender;
        private Label markedLabel;
        private int moveCount;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            towerA.BackColor = Color.Gray;
            towerB.BackColor = Color.Gray;
            towerC.BackColor = Color.Gray;

            button1.Click += Button1_Click;
            button2.Click += Button2_Click;
            button3.Click += Button3_Click;

            dragDropSender = null;
            Reset();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (dragDropSender == null) return;
            MoveDisk(dragDropSender.Value, Tower.C);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (dragDropSender == null) return;
            MoveDisk(dragDropSender.Value, Tower.B);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (dragDropSender == null) return;
            MoveDisk(dragDropSender.Value, Tower.A);
        }

        private void MoveDisk(Tower from, Tower to)
        {
            int disk = hanoi.Move(from, to);
            if (disk == -2)
            {
                MessageBox.Show("Die Platte kann nicht auf diesen Turm gelegt werden, da sie größer ist als die oberste auf diesem Turm.");
            } else if (disk > 0)
            {
                moveCount++;
                lblLastMove.Text = "Letzter Schritt: Lege Disk " + disk + " von Turm " + from.ToString() + " nach Turm " + to.ToString()
                +"              " + disk + ": " + from.ToString() + " > " + to.ToString();
            }
            ShowTowers();
        }

        private void Reset()
        {
            hanoi = new Hanoi(N);
            moveCount = 0;
            lblLastMove.Text = "Letzter Schritt:";
            btnSolve.Text = "schnellste Lösung berechnen";
            ShowTowers();
        }

        private void ClearTower(TableLayoutPanel tower)
        {
            while (tower.Controls.Count > 0)
            {
                tower.Controls.RemoveAt(0);
            }
        }

        private void ShowTowers()
        {
            lblMoveCount.Text = "Anzahl Schritte: " + moveCount;

            if (markedLabel != null)
            {
                markedLabel.BackColor = Color.LightGreen;
            }
            ClearTower(towerA);
            ClearTower(towerB);
            ClearTower(towerC);

            for (int i = 0; i < 3; i++)
            {
                List<int> disks = hanoi.GetDisks((Tower)i);
                disks.Reverse();

                for (int cnt = N; cnt > disks.Count; cnt--)
                {
                    switch (i)
                    {
                        case 0:
                            towerA.Controls.Add(CreateEmptyRow());
                            break;
                        case 1:
                            towerB.Controls.Add(CreateEmptyRow());
                            break;
                        case 2:
                            towerC.Controls.Add(CreateEmptyRow());
                            break;
                    }
                }

                bool first = true;
                foreach (int j in disks)
                {
                    switch (i)
                    {
                        case 0:
                            towerA.Controls.Add(CreateDisk(j, first));
                            break;
                        case 1:
                            towerB.Controls.Add(CreateDisk(j, first));
                            break;
                        case 2:
                            towerC.Controls.Add(CreateDisk(j, first));
                            break;
                    }
                    first = false;
                }
            }
            if (hanoi.IsFinished())
            {
                MessageBox.Show("Gratulation! Du hast das Problem in " + moveCount + " Schritten gelöst");
            }
        }

        private Control CreateDisk(int disk, bool clickable)
        {
            Panel p = CreateEmptyRow();
            Label l = new Label();
            l.Text = disk.ToString();
            l.Width = (int)((double)p.Width / (double)hanoi.N) * disk;
            l.Height = 25;
            l.Left = (300 - l.Width) / 2;
            l.BackColor = Color.LightGreen;
            l.TextAlign = ContentAlignment.MiddleCenter;
            l.Font = new Font(l.Font.FontFamily, 14);
            if (clickable)
            {
                l.MouseClick += L_MouseClick;
            }

            p.Controls.Add(l);
            return p;
        }

        private void L_MouseClick(object sender, MouseEventArgs e)
        {
            if (markedLabel != null)
            {
                markedLabel.BackColor = Color.LightGreen;
            }
            markedLabel = (Label)sender;
            markedLabel.BackColor = Color.Green;
            dragDropSender = GetTower((Label)sender);
        }


        private Tower GetTower(Label label)
        {
            if (label.Parent.Parent == towerA)
            {
                return Tower.A;
            }
            if (label.Parent.Parent == towerB)
            {
                return Tower.B;
            }
            return Tower.C;
        }

        private Panel CreateEmptyRow()
        {
            Panel p = new Panel();

            p.Width = 300;
            p.Height = 30;
            //p.BackColor = Color.Gray;

            return p;
        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            if (solver == null)
            {
                solver = new HanoiSolver(hanoi);
                solver.Solve();
                Reset();
                btnSolve.Text = "Nächster Schritt";
                return;
            }
            HanoiMove m = solver.Moves.First();
            MoveDisk(m.From, m.To);
            solver.Moves.RemoveAt(0);

            if (solver.Moves.Count == 0)
            {
                btnSolve.Text = "schnellste Lösung berechnen";
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void lblLastMove_Click(object sender, EventArgs e)
        {

        }

        private void lblMoveCount_Click(object sender, EventArgs e)
        {

        }
    }
}
