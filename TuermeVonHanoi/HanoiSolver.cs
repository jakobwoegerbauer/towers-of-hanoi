﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuermeVonHanoi {
    public class HanoiSolver {
        private Hanoi hanoi;
        public List<HanoiMove> Moves { get; private set; }

        public HanoiSolver(Hanoi hanoi)
        {
            this.hanoi = hanoi;
            this.Moves = new List<HanoiMove>();
        }

        public void Solve()
        {
            MoveDisks(Tower.A, Tower.C, hanoi.N);
        }

        private void MoveDisks(Tower from, Tower to, int amount)
        {
            if(amount == 0)
            {
                return;
            }
            MoveDisks(from, GetStagingTower(from, to), amount-1);
            MoveDisk(from, to);
            MoveDisks(GetStagingTower(from, to), to, amount - 1);
        }

        private void MoveDisk(Tower from, Tower to)
        {
            int disk = hanoi.Move(from, to);
            if(disk < 0)
            {
                throw new NotSupportedException("Could not move from " + from.ToString() + " to " + to.ToString());
            }
            Moves.Add(new HanoiMove(from, to, disk));
        }

        private Tower GetStagingTower(Tower from, Tower to)
        {
            if (Tower.A != from && Tower.A != to) return Tower.A;
            if (Tower.B != from && Tower.B != to) return Tower.B;
            return Tower.C;
        }
    }
}
