﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuermeVonHanoi {
    public class HanoiMove {
        public Tower From { get; set; }
        public Tower To { get; set; }
        public int Disk { get; set; }
        public HanoiMove(Tower from, Tower to, int disk)
        {
            this.From = from;
            this.To = to;
            this.Disk = disk;
        }
    }
}
